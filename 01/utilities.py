import numpy as np


class Problem:
    def __init__(self, ID, n, M, C, weights, prices, sorted=False):
        self.ID = ID
        self.n = n
        self.M = M
        self.C = C
        self.weights = weights
        self.prices = prices

        if sorted:
            self.sorted_idxs = np.argsort(self.prices)
            self.prices = self.prices[self.sorted_idxs][::-1]
            self.weights = self.weights[self.sorted_idxs][::-1]

        self.max_price = 0
        self.max_conf = np.zeros((self.n,), dtype=int)
        self.visited = 0

    def __str__(self):
        return str(vars(self))


class Solution:
    def __init__(self, ID: int, n: int, price: int, solution: list):
        self.ID = ID
        self.n = n
        self.price = price
        self.solutions = [solution]

    def __str__(self):
        return str(vars(self))

    def add_solution(self, solution: list):
        self.solutions.append(solution)


def brute_knapsack(p: Problem, conf, i, weight, price):
    if i == p.n:
        p.visited += 1
        if price >= p.max_price:
            p.max_price = price
            p.max_conf = conf.copy()
        return

    conf[i] = 1
    if np.sum(conf * p.weights) <= p.M:
        brute_knapsack(p, conf, i + 1, np.sum(conf * p.weights), np.sum(conf * p.prices))

    conf[i] = 0
    brute_knapsack(p, conf, i + 1, np.sum(conf * p.weights), np.sum(conf * p.prices))


def branch_bound(p: Problem, conf, i, weight, price):
    if i == p.n:
        p.visited += 1
        if price >= p.max_price:
            p.max_price = price
            p.max_conf = conf.copy()
        return

    conf[i] = 1
    upper_bound = np.sum(conf * p.prices) + np.sum(p.prices[i:])

    if (np.sum(conf * p.weights) <= p.M) and (upper_bound >= p.max_price):
        branch_bound(p, conf, i + 1, np.sum(conf * p.weights), np.sum(conf * p.prices))

    conf[i] = 0
    branch_bound(p, conf, i + 1, np.sum(conf * p.weights), np.sum(conf * p.prices))
