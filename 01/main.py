import numpy as np
import pandas as pd

from utilities import Problem, Solution, branch_bound, brute_knapsack


def process_problem(p, opt_price, knapsack_solver):
    knapsack_solver(p, p.max_conf, 0, 0, 0)
    time = 0
    status = "FAIL" if (opt_price != p.max_price) else "OK"
    passed = str(p.max_price >= p.C)

    if status is "FAIL":
        print('{:<5}{:<5}{:>10}{:>10}{:>10}{:>10}{:>10}{:>5}'
              .format(p.ID, status, opt_price, p.C, p.max_price, passed, p.visited, time))

    return [p.ID, status, opt_price, p.C, p.max_price, passed, p.visited, time]


def obtain_solutions(sol_lines):
    solutions = [None] * 510
    for i in range(0, len(sol_lines)):
        sol = list(map(int, sol_lines[i].split()))
        s = Solution(sol[0], sol[1], sol[2], sol[3:])
        id = int(sol[0])

        if solutions[id] is None:
            solutions[id] = s
        else:
            solutions[id].add_solution(sol[3:])
    return solutions


def solve_instances(inst_lines, solutions, method):
    results = pd.DataFrame(columns=["ID", "status", "opt price", "C", "max price", "Passed", "visited", "time"])
    for i in range(0, len(inst_lines)):
        inst = list(map(int, inst_lines[i].split()))
        p = Problem(-inst[0], inst[1], inst[2], inst[3], np.array(inst[4::2]), np.array(inst[5::2]), sorted=True)
        opt_price = solutions[p.ID].price
        res = process_problem(p, opt_price, method)
        results.loc[i] = res
    return results


def main():
    for num in [4, 10, 15]:
        for type in ["N", "Z"]:
            inst_lines = [line.rstrip('\n') for line in open(f"./data/{type}R/{type}R{num}_inst.dat")]
            sol_lines = [line.rstrip('\n') for line in open(f"./data/{type}R/{type}K{num}_sol.dat")]

            solutions = obtain_solutions(sol_lines)

            results_brute = solve_instances(inst_lines, solutions, brute_knapsack)
            print(results_brute)
            results_brute.to_csv(f"data/results/{type}R/brute/{type}R{num}_out.csv")

            results_BB = solve_instances(inst_lines, solutions, branch_bound)
            print(results_BB)
            results_BB.to_csv(f"data/results/{type}R/BB/{type}R{num}_out.csv")


if __name__ == '__main__':
    main()
