from src.utilities import Problem
import utilities as ut
from src.tabu_search import TabuSearchSATSolver, BruteForce
import os
import cProfile
import time

import numpy as np
import pandas as pd


def sat(satset='M'):

    c = 50
    s = 201

    optimas = ut.get_optimas(c, s, satset)
    n_sols = ut.get_sols(c, s)
    non_opt = 0
    results = []
    tabu_size = 4
    n_iterations = 100
    for num in optimas.index:

        filename = f"data/wuf-{satset}/wuf{c}-{s}-{satset}/wuf{c}-0{num}.mwcnf"
        inst_lines = [line.rstrip('\n') for line in open(filename)]
        p = Problem(inst_lines, id=num)

        res = {'id': num, 'optimal': optimas.loc[num]['weight']}

        solver = TabuSearchSATSolver(
            p, tabu_size, n_iterations, neighborhood_size=None, initial_state=num, max_score=res['optimal'])
        best, score = solver.run(verbose=False)

        if score > res['optimal']:
            print("WEIRD, score > optimal")

        res['tabu'] = score
        res['reached'] = solver.iteration_best.index(score) if score != 0 else len(solver.iteration_best)
        res['eps'] = ut.get_error(score, res['optimal'])
        if (score == 0):
            non_opt += 1
        res['n_sol'] = n_sols[num]
        print(res)
        results.append(res)

    res = pd.DataFrame(results)
    # os.makedirs(f"results/wuf-{satset}", exist_ok=True)
    # res.to_csv(f"results/wuf-{satset}/wuf{c}-{s}-{satset}-tabu-{tabu_size}-{n_iterations}.csv", index=False)


    msg = f"finished. Number of no-solutions, {non_opt}"
    print(msg)
    # os.system(f'say {msg}')


def parameters_search(c, s, rdseed, n_samples, satsets):
    for satset in satsets:
        optimas = ut.get_optimas(c, s, satset)
        sample = optimas.sample(n_samples, random_state=rdseed)
        problems = [get_problem(satset, c, s, idx) for idx in sample.index]
        print("SATSET: ", satset)
        results = []
        # for tabu_size in np.linspace(3, 12, 6, dtype=int):
        #     for n_iterations in np.linspace(500, 1000, 2, dtype=int):
        #         for nh_size in np.linspace(12, c, 5, dtype=int):
        for tabu_size in [4,5,10,20,50, 100]:
            print("tabu_size: ", tabu_size)
            for n_iterations in [1000]:
                for nh_size in [5,7,10,13,15,17,20]:
                    results.append(get_sample_stats(sample, problems, tabu_size, n_iterations, nh_size))
        df = pd.DataFrame(results)
        df.to_csv(f'./param_search/param-search-{satset}-s{s}-{n_samples}-{rdseed}-exhaustive.csv', index=False)


def get_problem(satset, c, s, num):
    filename = f"data/wuf-{satset}/wuf{c}-{s}-{satset}/wuf{c}-0{num}.mwcnf"
    inst_lines = [line.rstrip('\n') for line in open(filename)]
    return Problem(inst_lines, id=num)


def get_sample_stats(sample, problems, tabu_size, n_iterations, nh_size, verbose=True):
    errors = []
    iterations = []
    for p in problems:
        if p.n_vars > 20:
            print("FRAUD!", (p.id))
            continue
        solver = TabuSearchSATSolver(p, int(tabu_size), int(n_iterations),
                                     neighborhood_size=nh_size, initial_state=p.id, max_score=sample.loc[p.id].weight)
        best, score = solver.run(verbose=False)
        errors.append(ut.get_error(score, sample.loc[p.id].weight))
        iterations.append(solver.iteration_best.index(score) if score != 0 else len(solver.iteration_best))

    res = {
        'n_iterations': n_iterations,
        'tabu_size': tabu_size,
        'nh_size': nh_size,
        'max_relative_error': np.max(errors),
        'mean_relative_error': np.mean(errors),
        'std_relative_error': np.std(errors),
        'mean_reached': np.mean(iterations),
        'std_reached': np.std(iterations),
    }
    if verbose:
        print(res)
    return res


def main():
    # for s in [91, 88]:
    parameters_search(c=20, s=78, rdseed=87, n_samples=20, satsets=['M'])
    # sat()


if __name__ == '__main__':
    main()
