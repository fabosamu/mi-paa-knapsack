import numpy as np
import pandas as pd


class Problem:
    def __init__(self, lines, id=None):
        self.id = id
        self.n_vars = None
        self.conf = list()
        self.n_clauses = None
        self.clauses = list()
        self.meta_3clauses = np.array([], dtype=int)
        self.meta_truth = np.array([], dtype=bool)
        self.weights = None

        self.parse_mwcnf(lines)
        self.check_consistency()

    def check_consistency(self):
        pass

    def parse_mwcnf(self, lines):
        comments = []
        for line in lines:
            if "%" in line:
                return
            if line.startswith('c'):
                comments.append(line)
                continue
            if line.startswith('p'):
                self.n_vars, self.n_clauses = list(map(int, line.split()[2:4]))
                self.conf = np.zeros(self.n_vars, dtype=bool)
                continue
            if line.startswith('w'):
                self.weights = np.array(list(map(int, line.split()[1:-1])))
                continue

            r = np.array(list(map(int, line[:-2].split())))

            not_neg = r > 0
            r[r < 0] += 1
            r[r > 0] -= 1

            if len(r) != 3:
                raise RuntimeError("Clause does not contain 3 literals: " + str(comments))
            # print((self.meta_3clauses, np.abs(r)))
            self.meta_3clauses = np.concatenate((self.meta_3clauses, np.abs(r)))
            self.meta_truth = np.concatenate((self.meta_truth, not_neg))

            clause = dict(zip(np.abs(r), not_neg))
            # print(line[:-2])
            # print(clause)
            self.clauses.append(clause)

    def __str__(self):
        return str({
            "id": self.id,
            "n_vars": self.n_vars,
            "n_clauses": self.n_clauses,
            "clauses": self.clauses,
            "weights": self.weights})


def get_optimas(c, s, satset):
    filename = f"data/wuf-{satset}/wuf{c}-{s}-{satset}-opt.dat"
    opt_lines = [line.rstrip('\n') for line in open(filename)]
    optimas = []
    for line in opt_lines:
        split = line.split()
        optimas.append({"id": int(split[0][6:]), "weight": split[1], "cert": split[2:-1]})

    opt = pd.DataFrame(optimas)
    opt.id = opt.id.astype('int')
    opt.drop_duplicates(subset=['id'], keep='first', inplace=True)
    opt.weight = opt.weight.astype('int')
    opt = opt.set_index("id").sort_index()
    return opt


def get_sols(c, s):
    filename = f"data/uf-solnums/uf{c}-{s}-sols.csv"
    sols = pd.read_csv(filename, sep=';')
    sols['instance'] = sols['instance'].str.replace(f"uf{c}-0", '')
    sols['instance'] = sols['instance'].astype('int64')
    sols = sols.set_index('instance').sort_index()
    sols = sols.values.tolist()
    return [0] + [item for sublist in sols for item in sublist]


def get_error(score, optimal):
    if score == 0 and optimal == 0:
        return 0
    return np.abs(score - optimal) / max(score, optimal)
