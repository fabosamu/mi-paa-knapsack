from abc import ABC, abstractmethod
from copy import deepcopy
from collections import deque
import numpy as np
from src.utilities import Problem
from itertools import product


class TabuSearch(ABC):

    def __init__(self, p: Problem, tabu_size, max_steps,
                 neighborhood_size=None, initial_state='random', max_score=None):
        """
        :param initial_state: initial state, should implement __eq__ or __cmp__
        :param tabu_size: number of states to keep in tabu list
        :param max_steps: maximum number of steps to run algorithm for
        :param max_score: score to stop algorithm once reached
        """
        self.best_score = -1

        self.cur_steps = None
        self.tabu_list = None

        self.current = None
        self.best = None

        self.max_score = max_score

        self.iteration_best = list()
        if initial_state is 'random':
            self.initial_state = np.random.randint(2, size=p.n_vars, dtype=bool).tolist()
        elif type(initial_state) is int:
            np.random.seed(initial_state)
            self.initial_state = np.random.randint(2, size=p.n_vars, dtype=bool).tolist()
        elif len(initial_state) == p.n_vars:
            self.initial_state = initial_state
        else:
            raise TypeError("initial state must be seed, state of length c, or 'random'.")
        self.p = p

        if neighborhood_size is not None:
            self.neighborhood_size = neighborhood_size
        else:
            self.neighborhood_size = int(p.n_vars / 2)

        self.max_iter = 2**12

        if isinstance(tabu_size, int) and tabu_size > 0:
            self.tabu_size = tabu_size
        else:
            raise TypeError('Tabu size must be a positive integer')

        if isinstance(max_steps, int) and max_steps > 0:
            self.max_steps = max_steps
        else:
            raise TypeError('Maximum steps must be a positive integer')

    def __str__(self):
        return ('TABU SEARCH: \n' +
                'CURRENT STEPS: %d \n' +
                'BEST SCORE: %f \n' +
                'BEST MEMBER: %s \n\n') % \
               (self.cur_steps, self._score(self.best), str(self.best))

    def __repr__(self):
        return self.__str__()

    def _clear(self):
        """
        Resets the variables that are altered on a per-run basis of the algorithm
        :return: None
        """
        self.cur_steps = 0
        self.tabu_list = deque(maxlen=self.tabu_size)
        self.current = self.initial_state
        self.best = self.initial_state

    @abstractmethod
    def _score(self, state):
        """
        Returns objective function value of a state
        :param state: a state
        :return: objective function value of state
        """
        pass

    @abstractmethod
    def _neighborhood(self):
        """
        Returns list of all members of neighborhood of current state, given self.current
        :return: list of members of neighborhood
        """
        pass

    def _best(self, neighborhood):
        """
        Finds the best member of a neighborhood
        :param neighborhood: a neighborhood
        :return: best member of neighborhood
        """
        return neighborhood[np.argmax(np.array([self._score(x) for x in neighborhood]))]

    def run(self, verbose=True):
        """
        Conducts tabu search
        :param verbose: indicates whether or not to print progress regularly
        :return: best state and objective function value of best state
        """
        self._clear()
        i = 0
        steps = self.max_steps
        while i <= steps:
            i += 1
            self.cur_steps += 1

            neighborhood = self._neighborhood()
            neighborhood_best = self._best(neighborhood)

            while True:
                if all([x in self.tabu_list for x in neighborhood]):
                    if verbose:
                        print("TERMINATING - NO SUITABLE NEIGHBORS")
                    return self.best, self._score(self.best)
                if neighborhood_best in self.tabu_list:
                    if self._score(neighborhood_best) > self._score(self.best):
                        self.tabu_list.append(neighborhood_best)
                        self.best = deepcopy(neighborhood_best)
                        self.best_score = self._score(self.best)
                        break
                    else:
                        neighborhood.remove(neighborhood_best)
                        neighborhood_best = self._best(neighborhood)
                else:
                    self.tabu_list.append(neighborhood_best)
                    self.current = neighborhood_best
                    if self._score(self.current) > self._score(self.best):
                        self.best = deepcopy(self.current)
                        self.best_score = self._score(self.best)
                    break

            self.iteration_best.append(self._score(neighborhood_best))

            if self.max_score and self.best_score >= self.max_score:
                if verbose:
                    print("TERMINATING - REACHED MAXIMUM SCORE")
                return self.best, self.best_score
            if (i == steps) and (self._score(self.best) <= 0) and (steps <= self.max_iter):
                steps += int(self.max_steps * (1/2))
        if verbose:
            print("TERMINATING - REACHED MAXIMUM STEPS")
        return self.best, self._score(self.best)


class TabuSearchSATSolver(TabuSearch):
    def __init__(self, p: Problem, tabu_size, max_steps,
                 neighborhood_size=None, initial_state='random', max_score=None):
        super().__init__(p, tabu_size, max_steps, neighborhood_size, initial_state, max_score)

    @staticmethod
    def satisfiable(state, clauses):
        f = True
        for lit in clauses:
            acc = False
            for l in lit.keys():
                acc |= state[l] == lit[l]
            f &= acc
            if not f:
                return False
        return f

    def _score(self, state):
        if not self.satisfiable(state, self.p.clauses):
            return 0
        else:
            return int(sum(self.p.weights[state]))

    def _neighborhood(self):
        return self._neighborhood_random()
        # return self._neighborhood_admissible()
        # return _neighborhood_systematical_salted()

    def _neighborhood_systematical_salted(self):
        member = self.current
        neighborhood = []
        for i in range(0, len(member)):
            neighbor = member.copy()
            neighbor[i] = not member[i]
            neighborhood.append(list(neighbor))
        neighborhood = self.salt_neighborhood(neighborhood)
        return neighborhood

    def salt_neighborhood(self, nh):
        for i in range(len(nh)):
            pos = np.random.randint(0, len(nh[i]))
            nh[i][pos] = not nh[i][pos]
        return nh

    def _neighborhood_random(self):
        neighborhood = []
        member = self.current.copy()
        pos = np.random.choice(range(len(member)), int(self.neighborhood_size), replace=False)
        for p in pos:
            mem = member.copy()
            mem[p] = not mem[p]
            neighborhood.append(mem)
        return neighborhood

    def _neighborhood_admissible(self, length=1):
        member = self.current.copy()
        neighborhood = []
        while len(neighborhood) <= length:
            pos = np.random.randint(0, self.p.n_vars)
            member[pos] = not member[pos]
            if self.satisfiable(member, self.p.clauses):
                neighborhood.append(member)
        return neighborhood


class BruteForce(TabuSearchSATSolver):
    def __init__(self, p: Problem, initial_state, tabu_size, max_steps):
        super().__init__(p, initial_state, tabu_size, max_steps)

    def brute(self):
        max_weight = 0
        for state in list(map(
                lambda x: np.array(x, dtype=bool), list(product(range(2), repeat=self.p.n_vars)))):
            if self.satisfiable(state, self.p.clauses):
                weight = np.sum(self.p.weights[state])
                if weight >= max_weight:
                    max_weight = weight
        return max_weight

    def brute_(self, p: Problem, conf, i, weight, max_weight):
        if i == len(conf):
            # print(conf, weight)
            if weight >= max_weight:
                max_weight = weight
            return max_weight

        conf[i] = True
        # if self.satisfiable(conf, p.clauses):
        max_weight = self.brute_(p, conf, i + 1, int(sum(self.p.weights[conf])), max_weight)

        conf[i] = False
        max_weight = self.brute_(p, conf, i + 1, int(sum(self.p.weights[conf])), max_weight)

        return max_weight
