from src.utilities import Problem, get_optimas, get_sols
from src.tabu_search import TabuSearchSATSolver, BruteForce

import numpy as np
from itertools import product
import time


def satisfiable(state, clauses, truths):
    f = True
    c = False
    for i in range(len(clauses)):
        # if (i % 3 == 0) or (i % 3 == 1):
        c |= truths[i] == state[clauses[i]]
        if i % 3 == 2:
            f &= c
            c = False
        if f is False:
            return False
    return True


def sols():

    satset = 'M'


    c = 20
    s = 78
    n = 20

    sols = get_sols(c, s)
    # optimas = get_optimas(c, s, satset)


    results = []
    for num in range(1, n):
        filename = f"data/wuf-{satset}/wuf{c}-{s}-{satset}/wuf{c}-0{num}.mwcnf"
        inst_lines = [line.rstrip('\n') for line in open(filename)]
        p = Problem(inst_lines, id=num)
        # print(p.clauses)
        # print(p.meta_truth, p.meta_3clauses)

        state = np.zeros(p.n_vars + 1, dtype=bool).tolist()

        solver = TabuSearchSATSolver(p, state, 300, 1000)
        n_sat = 0
        start = time.process_time()
        for state in list(map(
                lambda x: np.array(x, dtype=bool), list(product(range(2), repeat=p.n_vars)))):

            # sat = solver.satisfiable(state, p.clauses)

            sat = satisfiable(state, p.meta_3clauses, p.meta_truth)

            # print(state, sat)
            n_sat += sat
        end = time.process_time()
        print(f"my: {n_sat}, true: {sols[num]}, time: {end-start}")
        if n_sat != sols[num]:
            print("SI KOKOT")


def neighbors():
    member = [True, False, True, False]
    neighborhood = []
    for i in range(0, len(member)):
        neighbor = member.copy()
        neighbor[i] = not member[i]
        neighborhood.append(list(neighbor))
    print("neighborhood:", neighborhood)


def example():

    inst_lines = [line.rstrip('\n') for line in open("data/example1.cnf")]
    inst_lines = [line.rstrip('\n') for line in open("data/example.cnf")]
    p = Problem(inst_lines, id=0)
    print(p)

    state = np.zeros(p.n_vars + 1, dtype=bool).tolist()

    solver = TabuSearchSATSolver(p, state, 300, 1000)
    n_sat = 0
    for state in list(map(
            lambda x: np.array(x, dtype=bool), list(product(range(2), repeat=p.n_vars)))):
        sat = solver.satisfiable(state, p.clauses)
        # sat = satisfiable(state, p.meta_3clauses, p.meta_truth)
        print(state, sat)
        n_sat += sat
    print(n_sat)



    # state = np.random.randint(2, size=p.n_vars, dtype=bool).tolist()
    # # print(state)
    #
    # res = {'optimal': 8}
    #
    # # solver = BruteForce(p, state, 10, 100)
    # # score_brute = solver.brute(p, state, 1, 0, 0)
    # # res['brute'] = score_brute
    #
    # solver = TabuSearchSATSolver(p, state, 300, 1000)
    # best, score = solver.run(verbose=False)
    # print(score, best)
    #
    # res['tabu'] = score
    # print(res)


if __name__ == '__main__':
    print("started...")
    # sols()
    # example()
    neighbors()
