import numpy as np
import pandas as pd
from utilities import Solution, Problem, throw_out_large_items
from tabu_search import TabuSearchKnapsackSolver


def obtain_solutions(sol_lines):
    solutions = [None] * 5000
    for i in range(0, len(sol_lines)):
        sol = list(map(int, sol_lines[i].split()))
        s = Solution(sol[0], sol[1], sol[2], sol[3:])
        id = int(sol[0])

        if solutions[id] is None:
            solutions[id] = s
        else:
            solutions[id].add_solution(sol[3:])
    return solutions


def solve_instances(inst_lines, solutions, solver_c):
    results = []
    print("|" * len(inst_lines))
    for i in range(0, len(inst_lines)):
        print("|", end="")
        inst = list(map(int, inst_lines[i].split()))
        p = Problem(inst[0], inst[1], inst[2], np.array(inst[3::2]), np.array(inst[4::2]))

        solver = solver_c(p)
        results.append(solver.solve_knapsack(solutions[p.ID].price))
    print()
    return pd.DataFrame(results)


def main():
    for group in [
        "NK",
        "ZKC",
        "ZKW"
        ]:
        for num in [32, 37]:
            inst_lines = [line.rstrip('\n') for line in open(f"./data/{group}/{group}{num}_inst.dat")]
            sol_lines = [line.rstrip('\n') for line in open(f"./data/{group}/{group}{num}_sol.dat")]

            solutions = obtain_solutions(sol_lines)

            N = len(inst_lines)
            N = 50 if len(inst_lines) > 50 else len(inst_lines)

            idxs = np.random.choice(len(inst_lines), N, replace=False)

            tabu_size = 50
            max_steps = 300

            results = []
            print("|" * N)
            for i in range(0, N):
                inst = list(map(int, inst_lines[i].split()))
                p_ = Problem(inst[0], inst[1], inst[2], np.array(inst[3::2]), np.array(inst[4::2]))
                p = throw_out_large_items(p_)

                solver = TabuSearchKnapsackSolver(p, p.max_conf, tabu_size, max_steps)
                best, score = solver.run(verbose=False)

                # print(str(np.array(best, dtype=int)) + " " + str(score))
                # print(np.array(solutions[p.ID].solutions[0]), solutions[p.ID].price)
                if np.sum(p.weights[best]) > p.M:
                    raise RuntimeWarning(p.ID)
                if solutions[p.ID].price != 0:
                    eps = abs(score - solutions[p.ID].price) / max(score, solutions[p.ID].price)
                else:
                    eps = 0.
                results.append({"optimal": solutions[p.ID].price, "best": score, 'status': int(score == solutions[p.ID].price), "eps": eps})
                print("|", end="")
            print()
            results = pd.DataFrame(results)

            succ = results.status.mean() * 100
            print(f"Success: {succ}%")
            print(f"Relative error eps: {results.eps.max()}")
            print(f"Average Relative error eps: {results.eps.mean()}")

            results.to_csv(f"data/results/{group}/{group}{num}_out.csv", index=False)


if __name__ == '__main__':
    main()

