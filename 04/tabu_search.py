from abc import ABC, abstractmethod
from copy import deepcopy
from collections import deque
import numpy as np
from utilities import Problem


class TabuSearch(ABC):

    def __init__(self, p: Problem, initial_state, tabu_size, max_steps, max_score=None):
        """
        :param initial_state: initial state, should implement __eq__ or __cmp__
        :param tabu_size: number of states to keep in tabu list
        :param max_steps: maximum number of steps to run algorithm for
        :param max_score: score to stop algorithm once reached
        """

        self.cur_steps = None
        self.tabu_list = None

        self.current = None
        self.best = None

        self.max_score = None

        self.iteration_best = list()

        self.initial_state = initial_state
        self.p = p

        if isinstance(tabu_size, int) and tabu_size > 0:
            self.tabu_size = tabu_size
        else:
            raise TypeError('Tabu size must be a positive integer')

        if isinstance(max_steps, int) and max_steps > 0:
            self.max_steps = max_steps
        else:
            raise TypeError('Maximum steps must be a positive integer')

        if max_score is not None:
            if isinstance(max_score, (int, float)):
                self.max_score = float(max_score)
            else:
                raise TypeError('Maximum score must be a numeric type')

    def __str__(self):
        return ('TABU SEARCH: \n' +
                'CURRENT STEPS: %d \n' +
                'BEST SCORE: %f \n' +
                'BEST MEMBER: %s \n\n') % \
               (self.cur_steps, self._score(self.best), str(self.best))

    def __repr__(self):
        return self.__str__()

    def _clear(self):
        """
        Resets the variables that are altered on a per-run basis of the algorithm
        :return: None
        """
        self.cur_steps = 0
        self.tabu_list = deque(maxlen=self.tabu_size)
        self.current = self.initial_state
        self.best = self.initial_state

    @abstractmethod
    def _score(self, state):
        """
        Returns objective function value of a state
        :param state: a state
        :return: objective function value of state
        """
        pass

    @abstractmethod
    def _neighborhood(self):
        """
        Returns list of all members of neighborhood of current state, given self.current
        :return: list of members of neighborhood
        """
        pass

    def _best(self, neighborhood):
        """
        Finds the best member of a neighborhood
        :param neighborhood: a neighborhood
        :return: best member of neighborhood
        """
        return neighborhood[np.argmax(np.array([self._score(x) for x in neighborhood]))]

    def run(self, verbose=True):
        """
        Conducts tabu search
        :param verbose: indicates whether or not to print progress regularly
        :return: best state and objective function value of best state
        """
        self._clear()
        for i in range(self.max_steps):
            self.cur_steps += 1

            if ((i + 1) % 100 == 0) and verbose:
                print(self)

            neighborhood = self._neighborhood()
            if not neighborhood:
                continue
            neighborhood_best = self._best(neighborhood)

            while True:
                if all([x in self.tabu_list for x in neighborhood]):
                    if verbose:
                        print("TERMINATING - NO SUITABLE NEIGHBORS")
                    return self.best, self._score(self.best)
                if neighborhood_best in self.tabu_list:
                    if self._score(neighborhood_best) > self._score(self.best):
                        self.tabu_list.append(neighborhood_best)
                        self.best = deepcopy(neighborhood_best)
                        break
                    else:
                        neighborhood.remove(neighborhood_best)
                        neighborhood_best = self._best(neighborhood)
                else:
                    self.tabu_list.append(neighborhood_best)
                    self.current = neighborhood_best
                    if self._score(self.current) > self._score(self.best):
                        self.best = deepcopy(self.current)
                    break

            # if self.max_score is not None and self._score(self.best) > self.max_score:
            #     if verbose:
            #         print("TERMINATING - REACHED MAXIMUM SCORE")
            #     return self.best, self._score(self.best)
            self.iteration_best.append(self._score(neighborhood_best))
        if verbose:
            print("TERMINATING - REACHED MAXIMUM STEPS")
        return self.best, self._score(self.best)


class TabuSearchKnapsackSolver(TabuSearch):

    def __init__(self, p: Problem, initial_state, tabu_size, max_steps):
        super().__init__(p, initial_state, tabu_size, max_steps)

    def _score(self, state):
        if sum(self.p.weights[state]) > self.p.M:
            return -1
        else:
            return int(sum(self.p.prices[state]))

    def _neighborhood(self):
        member = self.current
        neighborhood = []
        for i in range(len(member)):
            neighbor = member.copy()
            neighbor[i] = ~member[i]
            # if sum(self.p.weights[neighbor]) <= self.p.M:
                # print("yep, there are also bad configs.")
                # neighborhood.append(list(neighbor))
            neighborhood.append(list(neighbor))
        # print("neighborhood:", neighborhood)
        return neighborhood

