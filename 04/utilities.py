import numpy as np


class Problem:
    def __init__(self, ID, n, M, weights, prices):
        self.ID = ID
        self.n = n
        self.M = M
        self.weights = weights
        self.prices = prices
        self.max_price = 0
        self.max_conf = np.zeros((self.n,), dtype=bool)
        self.visited = 0

    def __str__(self):
        return str(vars(self))

    def sort_by_price(self, desc=True):
        self.sorted_idxs = np.argsort(self.prices)
        # ASC
        self.prices = self.prices[self.sorted_idxs]
        # appropriate order of weights to prices
        self.weights = self.weights[self.sorted_idxs]
        if desc:
            self.prices = self.prices[::-1]
            self.weights = self.weights[::-1]


def throw_out_large_items(p: Problem):
    n = p.n
    wt = list()
    pc = list()

    for i in range(p.n):
        if p.weights[i] > p.M:
            n -= 1
            continue
        else:
            wt.append(p.weights[i])
            pc.append(p.prices[i])

    weights = np.array(wt)
    prices = np.array(pc)
    problem = Problem(p.ID, n, p.M, weights, prices)
    return problem
    # def getBaseConfiguration(self):
    #     # return State(self.weights, self.prices, self.max_conf)
    #     return np.array(self.max_conf, dtype=bool)


class State:
    def __init__(self, weights, prices, conf):
        self.w = np.array(weights)
        self.p = np.array(prices)
        self.conf = np.array(conf, dtype=bool)

    def cost(self):
        return np.sum(self.p[self.conf])

    def __eq__(self, other):
        return np.equal(self.conf, other.conf)

class Solution:
    def __init__(self, ID: int, n: int, price: int, solution: list):
        self.ID = ID
        self.n = n
        self.price = price
        self.solutions = [solution]

    def __str__(self):
        return str(vars(self))

    def add_solution(self, solution: list):
        self.solutions.append(solution)

