import numpy as np
import pandas as pd
import os

from utilities import Problem, Solution
import solvers as S

def obtain_solutions(sol_lines):
    solutions = [None] * 5000
    for i in range(0, len(sol_lines)):
        sol = list(map(int, sol_lines[i].split()))
        s = Solution(sol[0], sol[1], sol[2], sol[3:])
        id = int(sol[0])

        if solutions[id] is None:
            solutions[id] = s
        else:
            solutions[id].add_solution(sol[3:])
    return solutions


def solve_instances(inst_lines, solutions, solver_c):
    results = []
    print("|" * len(inst_lines))
    for i in range(0, len(inst_lines)):
        print("|", end="")
        inst = list(map(int, inst_lines[i].split()))
        p = Problem(inst[0], inst[1], inst[2], np.array(inst[3::2]), np.array(inst[4::2]))

        solver = solver_c(p)
        results.append(solver.solve_knapsack(solutions[p.ID].price))
    return pd.DataFrame(results)


def main():
    for group in ["NK",
                  "ZKC",
                  "ZKW"
                  ]:
        for num in [20, 25]:
            inst_lines = [line.rstrip('\n') for line in open(f"./data/{group}/{group}{num}_inst.dat")]
            sol_lines = [line.rstrip('\n') for line in open(f"./data/{group}/{group}{num}_sol.dat")]

            solutions = obtain_solutions(sol_lines)

            for solver in [
                # S.Brute,
                # S.DPNaive,
                S.BranchBound,
                # S.DPIterative,
                # S.DPIterativeByPrice,
                # S.Greedy,
                # S.GreedyRedux,
            ]:
                # if os.path.exists(f"data/results/{group}K/{solver.name()}/{group}R{num}_out.csv"):
                #     break
                print("started ", solver.name())
                results = solve_instances(inst_lines, solutions, solver)
                succ = results.status.mean() * 100
                print(f"\nSuccess of {solver.name()}: {succ}%")

                # results.to_csv(f"data/results/{group}/{solver.name()}/{group}{num}_out.csv")

            # print("fptas started")
            # if os.path.exists(f"data/results/{group}K/{S.FPTAS.name()}/{group}R{num}_out.csv"):
            #     break
            # results_FPTAS = solve_instances(inst_lines, solutions, S.FPTAS)
            # results_FPTAS.to_csv(f"data/results/{group}/{S.FPTAS.name()}/{group}{num}_out.csv")
            # print("fptas finished")

            # import pandas as pd
            # df = pd.read_csv(f"data/results/{group}/{S.FPTAS.name()}/{group}{num}_out.csv")
            # print(df.max())



if __name__ == '__main__':
    main()
