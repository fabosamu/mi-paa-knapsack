from utilities import Problem
import numpy as np
import math


class Solver:

    def __init__(self, p: Problem):
        self.p = p

    def solve_knapsack(self, opt_price):
        self.solve()

        status = 0 if (opt_price != self.p.max_price) else 1
        if status is 0:
            print('{:<5}{:<5}{:>10}{:>10}{:>10}'
                  .format(self.p.ID, status, opt_price, self.p.max_price, self.p.visited))

        return {"ID": self.p.ID,
                "status": status,
                "opt price": opt_price,
                "max price": self.p.max_price,
                "visited": self.p.visited}

    def solve(self):
        raise NotImplementedError("Must implement abstract method.")

    @staticmethod
    def name():
        raise NotImplementedError("Must implement abstract method.")


class Brute(Solver):

    def solve(self):
        self.brute_knapsack(self.p, self.p.max_conf, 0, 0, 0)

    def brute_knapsack(self, p: Problem, conf, i, weight, price):
        if i == p.n:
            p.visited += 1
            if price >= p.max_price:
                p.max_price = price
                p.max_conf = conf.copy()
            return

        conf[i] = 1
        if np.sum(conf * p.weights) <= p.M:
            self.brute_knapsack(p, conf, i + 1, np.sum(conf * p.weights), np.sum(conf * p.prices))

        conf[i] = 0
        self.brute_knapsack(p, conf, i + 1, np.sum(conf * p.weights), np.sum(conf * p.prices))

    @staticmethod
    def name():
        return "Brute"


class BranchBound(Solver):

    def solve(self):
        self.p.sort_by_price()
        self.branch_bound(self.p, self.p.max_conf, 0, 0, 0)

    def branch_bound(self, p: Problem, conf, i, weight, price):
        if i == p.n:
            p.visited += 1
            if price >= p.max_price:
                p.max_price = price
                p.max_conf = conf.copy()
            return

        conf[i] = 1
        upper_bound = np.sum(conf * p.prices) + np.sum(p.prices[i:])

        if (np.sum(conf * p.weights) <= p.M) and (upper_bound >= p.max_price):
            self.branch_bound(p, conf, i + 1, np.sum(conf * p.weights), np.sum(conf * p.prices))

        conf[i] = 0
        self.branch_bound(p, conf, i + 1, np.sum(conf * p.weights), np.sum(conf * p.prices))

    @staticmethod
    def name():
        return "BB"


class DPNaive(Solver):

    def solve(self):
        W = self.p.M
        n = self.p.n
        self.p.max_price = self.knap_sack(W, self.p.weights, self.p.prices, n)

    def knap_sack(self, W, wt, val, n):

        # Base Case
        if n == 0 or W == 0:
            self.p.visited += 1
            return 0

        # If weight of the nth item is more than Knapsack of capacity
        # W, then this item cannot be included in the optimal solution
        if (wt[n - 1] > W):
            return self.knap_sack(W, wt, val, n - 1)

            # return the maximum of two cases:
        # (1) nth item included
        # (2) not included
        else:
            self.p.visited += 1
            return max(val[n - 1] + self.knap_sack(W - wt[n - 1], wt, val, n - 1),
                       self.knap_sack(W, wt, val, n - 1))

    @staticmethod
    def name():
        return "DP"


class DPIterative(Solver):

    def solve(self):
        W = self.p.M
        n = self.p.n
        self.p.max_price = self.knap_sack(W, self.p.weights, self.p.prices, n)

    def knap_sack(self, W, wt, val, n):
        K = [[0 for x in range(W + 1)] for x in range(n + 1)]

        # Build table K[][] in bottom up manner
        for i in range(n + 1):
            for w in range(W + 1):
                self.p.visited += 1
                if i == 0 or w == 0:
                    K[i][w] = 0
                elif wt[i - 1] <= w:
                    K[i][w] = max(val[i - 1] + K[i - 1][w - wt[i - 1]], K[i - 1][w])
                else:
                    K[i][w] = K[i - 1][w]

        return K[n][W]

    @staticmethod
    def name():
        return "DP_it"


class DPIterativeByPrice(Solver):

    def solve(self):
        self.p.max_price, self.p.max_conf = self.knap_sack(self.p.M, self.p.weights, self.p.prices, self.p.n)

    @staticmethod
    def knap_sack(M, wt, pc, n):
        sum_prices = np.sum(pc)
        K = np.zeros((sum_prices + 1, n + 1))

        for i in range(n + 1):
            for thing in range(sum_prices + 1):
                if thing == 0:
                    continue
                if i == 0:
                    K[thing][i] = math.inf
                    continue

                # item = things[i - 1]
                if thing == 1:
                    K[thing][i] = wt[i - 1] if thing == pc[i - 1] else math.inf
                    continue

                previous_weight = K[thing][i - 1]
                index = thing - pc[i - 1]

                if index < 0:
                    K[thing][i] = math.inf if previous_weight == math.inf else previous_weight
                else:
                    if K[index][i - 1] == math.inf:
                        K[thing][i] = min(previous_weight, K[index][i - 1])
                    else:
                        K[thing][i] = min(previous_weight, K[index][i - 1] + wt[i - 1])

        # Counting of best price
        best_price = 0
        local_i = 0
        for i in range(sum_prices, -1, -1):
            local_i = i
            if K[i][n] <= M:
                best_price = i
                break

        # Creating configuration list with including of things
        includes = np.zeros(n, dtype=int)

        c = n
        while local_i > 0:
            if K[local_i][c] == K[local_i][c - 1]:
                c -= 1
                continue

            local_i -= pc[c - 1]
            c -= 1
            includes[c] = 1

        # self.p.max_price = best_price
        # self.p.max_conf = includes
        return best_price, includes

    @staticmethod
    def name():
        return "DP_it_P"


class Greedy(Solver):
    def solve(self):
        self.p.max_price = self.greedy_solver()

    #         Začněte s věcí, která má toto kritérium nejlepší, a v každém kroku vložte do batohu věc s další nejlepší hodnotou, pokud se do batohu ještě vejde.
    #         Pokračujte až do vyčerpání všech věcí (některé další věci v seznamu mohou být tak malé, že se do batohu ještě vejdou, zatímco dřívější nikoliv).
    def greedy_solver(self):
        M = self.p.M
        idxs = np.argsort(self.p.prices / self.p.weights)[::-1]
        wt = self.p.weights[idxs]
        pc = self.p.prices[idxs]
        max_price = 0
        for i in range(len(wt)):
            self.p.visited += 1
            if M >= wt[i]:
                max_price = max_price + pc[i]
                M -= wt[i]
            else:
                return max_price
        return max_price

    @staticmethod
    def name():
        return "Greedy"


class GreedyRedux(Greedy):

    def solve(self):
        greedy_max_price = self.greedy_solver()
        solo_max_price = self.redux()

        self.p.max_price = max(greedy_max_price, solo_max_price)

    def redux(self):
        M = self.p.M
        idxs = np.argsort(self.p.prices)[::-1]
        pc = self.p.prices[idxs]
        wt = self.p.weights[idxs]
        for i in range(len(pc)):
            self.p.visited += 1
            if wt[i] <= M:
                return pc[i]
        return 0

    @staticmethod
    def name():
        return "Redux"


class FPTAS():

    def __init__(self, p: Problem, eps=(0.01, 0.05, 0.1, 0.2)):
        self.eps = eps
        self.p = self.throw_out_large_items(p)

    def solve_knapsack(self, opt_price):
        res = {"ID": self.p.ID,
               "opt price": opt_price}
        for eps in self.eps:
            # print("for eps:", eps)

            if self.p.n == 0:
                res.update({f"{eps} max price": 0, f"{eps} err:": 0})
                continue

            max_price = self.solve(eps)

            rel_err = abs( self.p.max_price - opt_price ) / max( self.p.max_price, opt_price )
            # rel_qlt = max( opt_price/self.p.max_price, self.p.max_price/opt_price )

            res.update({f"{eps} max price": max_price, f"{eps} err:": rel_err})

        return res

    def solve(self, eps):
        if self.p.n == 0:
            return 0
        C_M = max(self.p.prices)
        K = (eps * C_M) / self.p.n
        prices_red = np.array(np.floor(self.p.prices / K), dtype=int)
        # prices_red = self.p.prices
        price, conf = DPIterativeByPrice.knap_sack(self.p.M, self.p.weights, prices_red, self.p.n)

        max_price = np.sum(self.p.prices[np.array(conf, dtype=bool)])
        self.p.max_price = max_price

        return max_price

    @staticmethod
    def name():
        return "FPTAS"

    def throw_out_large_items(self, p: Problem):
        n = p.n
        wt = list()
        pc = list()

        for i in range(p.n):
            if p.weights[i] > p.M:
                n -= 1
                continue
            else:
                wt.append(p.weights[i])
                pc.append(p.prices[i])

        p.weights = np.array(wt)
        p.prices = np.array(pc)
        p.n = n
        return p