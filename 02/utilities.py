import numpy as np


class Problem:
    def __init__(self, ID, n, M, weights, prices):
        self.ID = ID
        self.n = n
        self.M = M
        self.weights = weights
        self.prices = prices
        self.max_price = 0
        self.max_conf = np.zeros((self.n,), dtype=int)
        self.visited = 0

    def __str__(self):
        return str(vars(self))

    def sort_by_price(self, desc=True):
        self.sorted_idxs = np.argsort(self.prices)
        # ASC
        self.prices = self.prices[self.sorted_idxs]
        # appropriate order of weights to prices
        self.weights = self.weights[self.sorted_idxs]
        if desc:
            self.prices = self.prices[::-1]
            self.weights = self.weights[::-1]

class Solution:
    def __init__(self, ID: int, n: int, price: int, solution: list):
        self.ID = ID
        self.n = n
        self.price = price
        self.solutions = [solution]

    def __str__(self):
        return str(vars(self))

    def add_solution(self, solution: list):
        self.solutions.append(solution)
