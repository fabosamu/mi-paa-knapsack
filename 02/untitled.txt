n =  # n of items
N =  # N of instances
W =  # max weight
C =  # max value

m =  # Capacity to sum of weights ratio as a fixed value or interval of the uniform distribution
k =  # granularity

ws = ['bal', 'light', 'heavy']   # majority of light|heavy items
cs = ['uni', 'corr', 'strong']   # correlation with weights uni|corr|strong
